from fastapi import FastAPI, Depends

from auth.jwt_bearer import JWTBearer
from routes.product import router as ProductRouter
from routes.admin import router as UserRouter

app = FastAPI()

token_listener = JWTBearer()

@app.get("/", tags=["Root"])
async def read_root():
    return {"message": "This is a test for an awesome company Qitech"}


app.include_router(UserRouter, tags=["User"], prefix="/user")
app.include_router(ProductRouter, tags=["Product"], prefix="/Product", dependencies=[Depends(token_listener)])
