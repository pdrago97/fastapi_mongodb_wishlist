from fastapi import APIRouter, Body
from fastapi.encoders import jsonable_encoder

from database.database import *
from models.product import *

router = APIRouter()


@router.get("/", response_description="products retrieved")
async def get_products():
    products = await retrieve_products()
    return ResponseModel(products, "Products data retrieved successfully") \
        if len(products) > 0 \
        else ResponseModel(
        products, "Empty list returned")


@router.get("/{id}", response_description="Product data retrieved")
async def get_product_data(id):
    product = await retrieve_product(id)
    return ResponseModel(product, "product data retrieved successfully") \
        if product \
        else ErrorResponseModel("An error occured.", 404, "product doesn't exist.")


@router.post("/", response_description="Product data added into the database")
async def add_Product_data(product: ProductModel = Body(...)):
    product = jsonable_encoder(product)
    new_product = await add_product(product)
    return ResponseModel(new_product, "product added successfully.")


@router.delete("/{id}", response_description="Student data deleted from the database")
async def delete_product_data(id: str):
    deleted_product = await delete_product(id)
    return ResponseModel("Student with ID: {} removed".format(id), "Product deleted successfully") \
        if deleted_product \
        else ErrorResponseModel("An error occured", 404, "Product with id {0} doesn't exist".format(id))


@router.put("{id}")
async def update_product(id: str, req: UpdateProductModel = Body(...)):
    updated_product = await update_product_data(id, req.dict())
    return ResponseModel("Product with ID: {} name update is successful".format(id),
                         "Product name updated successfully") \
        if updated_product \
        else ErrorResponseModel("An error occurred", 404, "There was an error updating the student.".format(id))
