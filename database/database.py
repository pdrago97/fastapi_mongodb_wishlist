import motor.motor_asyncio
from bson import ObjectId
from decouple import config

from .database_helper import student_helper, admin_helper

MONGO_DETAILS = config('MONGO_DETAILS')

client = motor.motor_asyncio.AsyncIOMotorClient(MONGO_DETAILS)

database = client.wishlist_database

products_collection = database.get_collection('products_collection')
admin_collection = database.get_collection('admins')

try:
    print(client.server_info())
except Exception:
    print("Unable to connect to the server.")

async def add_admin(admin_data: dict) -> dict:
    admin = await admin_collection.insert_one(admin_data)
    new_admin = await admin_collection.find_one({"_id": admin.inserted_id})
    return admin_helper(new_admin)

async def retrieve_products():
    products = []
    async for product in products_collection.find():
        products.append(student_helper(product))
    return products


async def add_product(product_data: dict) -> dict:
    product = await products_collection.insert_one(product_data)
    new_product = await products_collection.find_one({"_id": product.inserted_id})
    return student_helper(new_product)


async def retrieve_product(id: str) -> dict:
    product = await products_collection.find_one({"_id": ObjectId(id)})
    if product:
        return student_helper(product)


async def delete_product(id: str):
    product = await products_collection.find_one({"_id": ObjectId(id)})
    if product:
        await products_collection.delete_one({"_id": ObjectId(id)})
        return True


async def update_product_data(id: str, data: dict):
    product = await products_collection.find_one({"_id": ObjectId(id)})
    if product:
        products_collection.update_one({"_id": ObjectId(id)}, {"$set": data})
        return True
