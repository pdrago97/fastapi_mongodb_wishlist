from pydantic import BaseModel, Field, EmailStr


class AdminModel(BaseModel):
    fullname: str = Field(...)
    email: EmailStr = Field(...)
    password: str = Field(...)

    class Config:
        schema_extra = {
            "example": {
                "fullname": "Pedro Drago Reichow",
                "email": "pedroreichow3@gmail.com",
                "password": "Your_password_here"
            }
        }