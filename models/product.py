from typing import Optional

from pydantic import BaseModel, EmailStr, Field


class ProductModel(BaseModel):
    title: str = Field(...)
    description: str = Field(...)
    price: float = Field(...)
    available_stock: int = Field(...)
    image: Optional[str] = Field(None)
    link: Optional[str] = Field(None)

    class Config:
        schema_extra = {
            "example": {
                "title": "Product title",
                "description": "Product description",
                "price": "Water resources engineering",
                "available_stock": 2,
                "image": "your image url",
                "link": "3.0"
            }
        }


class UpdateProductModel(BaseModel):
    title: str = Field(...)
    description: str = Field(...)
    price: float = Field(...)
    available_stock: int = Field(...)
    image: Optional[str] = Field(None)
    link: Optional[str] = Field(None)

    class Config:
        schema_extra = {
            "example": {
                "title": "Product title",
                "description": "Product description",
                "price": "Water resources engineering",
                "available_stock": 2,
                "image": "your image url",
                "link": "3.0"
            }
        }


def ResponseModel(data, message):
    return {
        "data": [
            data
        ],
        "code": 200,
        "message": message,
    }


def ErrorResponseModel(error, code, message):
    return {
        "error": error,
        "code": code,
        "message": message
    }
